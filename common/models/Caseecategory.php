<?php

namespace common\models;

use Yii;
use \common\models\base\Caseecategory as BaseCaseecategory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "caseecategory".
 */
class Caseecategory extends BaseCaseecategory
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
