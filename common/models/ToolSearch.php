<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tool;

/**
* ToolSearch represents the model behind the search form about `common\models\Tool`.
*/
class ToolSearch extends Tool
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'created_at', 'updated_at', 'onmain', 'brif_id'], 'integer'],
            [['name', 'seokeywords', 'seotitle', 'seoh1', 'alias', 'image', 'previewimage', 'introtext', 'seodescription', 'html', 'content'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Tool::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'onmain' => $this->onmain,
            'brif_id' => $this->brif_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'seokeywords', $this->seokeywords])
            ->andFilterWhere(['like', 'seotitle', $this->seotitle])
            ->andFilterWhere(['like', 'seoh1', $this->seoh1])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'previewimage', $this->previewimage])
            ->andFilterWhere(['like', 'introtext', $this->introtext])
            ->andFilterWhere(['like', 'seodescription', $this->seodescription])
            ->andFilterWhere(['like', 'html', $this->html])
            ->andFilterWhere(['like', 'content', $this->content]);

return $dataProvider;
}
}