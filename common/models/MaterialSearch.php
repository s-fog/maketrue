<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Material;

/**
* MaterialSearch represents the model behind the search form about `common\models\Material`.
*/
class MaterialSearch extends Material
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'seokeywords', 'seotitle', 'seoh1', 'seodescription', 'alias', 'category', 'image', 'previewimage', 'introtext', 'content'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Material::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'seokeywords', $this->seokeywords])
            ->andFilterWhere(['like', 'seotitle', $this->seotitle])
            ->andFilterWhere(['like', 'seoh1', $this->seoh1])
            ->andFilterWhere(['like', 'seodescription', $this->seodescription])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'previewimage', $this->previewimage])
            ->andFilterWhere(['like', 'introtext', $this->introtext])
            ->andFilterWhere(['like', 'content', $this->content]);

return $dataProvider;
}
}