<?php

namespace common\models;

use Yii;
use \common\models\base\Brif as BaseBrif;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "brif".
 */
class Brif extends BaseBrif
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }


    public static function items() {
        return Brif::find()
            ->orderBy(['order' => SORT_ASC])
            ->all();
    }
}
