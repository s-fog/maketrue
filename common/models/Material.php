<?php

namespace common\models;

use Yii;
use \common\models\base\Material as BaseMaterial;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "material".
 */
class Material extends BaseMaterial
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public static function items() {
        return Material::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }
}
