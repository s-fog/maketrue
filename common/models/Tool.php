<?php

namespace common\models;

use Yii;
use \common\models\base\Tool as BaseTool;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tool".
 */
class Tool extends BaseTool
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public static function items($mode = '') {
        $where = '';

        if ($mode == 'onmain') {
            $where = ['onmain' => 1];
        }

        return Tool::find()
            ->orderBy(['order' => SORT_ASC])
            ->where($where)
            ->all();
    }
}
