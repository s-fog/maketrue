<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Casee;

/**
* CaseeSearch represents the model behind the search form about `common\models\Casee`.
*/
class CaseeSearch extends Casee
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'content', 'image', 'task', 'solution', 'analitics', 'icons', 'header2', 'content2', 'seokeywords', 'seotitle', 'seoh1', 'seodescription'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Casee::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'task', $this->task])
            ->andFilterWhere(['like', 'solution', $this->solution])
            ->andFilterWhere(['like', 'analitics', $this->analitics])
            ->andFilterWhere(['like', 'icons', $this->icons])
            ->andFilterWhere(['like', 'header2', $this->header2])
            ->andFilterWhere(['like', 'content2', $this->content2])
            ->andFilterWhere(['like', 'seokeywords', $this->seokeywords])
            ->andFilterWhere(['like', 'seotitle', $this->seotitle])
            ->andFilterWhere(['like', 'seoh1', $this->seoh1])
            ->andFilterWhere(['like', 'seodescription', $this->seodescription]);

return $dataProvider;
}
}