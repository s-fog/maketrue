<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\BrifSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="brif-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'seokeywords') ?>

		<?= $form->field($model, 'seotitle') ?>

		<?= $form->field($model, 'seoh1') ?>

		<?php // echo $form->field($model, 'seodescription') ?>

		<?php // echo $form->field($model, 'alias') ?>

		<?php // echo $form->field($model, 'image') ?>

		<?php // echo $form->field($model, 'previewimage') ?>

		<?php // echo $form->field($model, 'introtext') ?>

		<?php // echo $form->field($model, 'html') ?>

		<?php // echo $form->field($model, 'content') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
