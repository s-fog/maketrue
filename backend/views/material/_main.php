<?php
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;
use kartik\checkbox\CheckboxX;
use xtarantulz\preview\PreviewAsset;
PreviewAsset::register($this);
?>
    <br>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <!-- attribute alias -->
<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <!-- attribute image -->
<?= $form->field($model, 'image')->widget(InputFile::className(), [
    'language'      => 'ru',
    'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
    'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
    'options'       => ['class' => 'form-control img'],
    'buttonOptions' => ['class' => 'btn btn-success'],
    'multiple'      => false       // возможность выбора нескольких файлов
]) ?>

<?= $form->field($model, 'previewimage')->widget(InputFile::className(), [
    'language'      => 'ru',
    'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
    'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
    'options'       => ['class' => 'form-control img'],
    'buttonOptions' => ['class' => 'btn btn-success'],
    'multiple'      => false       // возможность выбора нескольких файлов
]) ?>

    <!-- attribute introtext -->
<?= $form->field($model, 'introtext')->textarea(['rows' => 10]) ?>

    <!-- attribute content -->
<?= $form->field($model, 'content')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>

    <!-- attribute content -->
<?= $form->field($model, 'content2')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>
