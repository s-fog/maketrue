<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Кейсы', 'url' => '#', 'items' => [
                        ['label' => 'Категории', 'url' => ['/caseecategory/index']],
                        ['label' => 'Кейсы', 'url' => ['/casee/index']],
                    ]],
                    ['label' => 'Инструменты', 'url' => ['/tool/index']],
                    ['label' => 'Брифы', 'url' => ['/brif/index']],
                    ['label' => 'Материалы', 'url' => ['/material/index']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                ],
            ]
        ) ?>

    </section>

</aside>
