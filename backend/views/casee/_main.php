<?php
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;
use xtarantulz\preview\PreviewAsset;
PreviewAsset::register($this);
?>
    <br>
<?=$form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?=$form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

<?=$form->field($model, 'order')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'show_in_list')->widget(CheckboxX::classname(), [
    'pluginOptions' => [
        'threeState'=>false
    ]
])?>

<!-- attribute content -->
<?= $form->field($model, 'content')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>

<?= $form->field($model, 'introtext')->textarea() ?>

<?=$form->field($model, 'caseecategory_id')->widget(Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\common\models\Caseecategory::find()->all(), 'id', 'name'),
    'options' => ['placeholder' => 'Выберите категорию ...']
]);?>

<!-- attribute task -->
<?= $form->field($model, 'task')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>

<!-- attribute solution -->
<?= $form->field($model, 'solution')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>

<!-- attribute analitics -->
<?= $form->field($model, 'analitics')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>

<!-- attribute icons -->
<?= $form->field($model, 'icons')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>

    <!-- attribute header2 -->
<?= $form->field($model, 'header2')->textInput(['maxlength' => true]) ?>

<!-- attribute content2 -->
<?= $form->field($model, 'content2')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'standart'
    ],
]) ?>