<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\CaseeSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="casee-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'content') ?>

		<?= $form->field($model, 'image') ?>

		<?= $form->field($model, 'task') ?>

		<?php // echo $form->field($model, 'solution') ?>

		<?php // echo $form->field($model, 'analitics') ?>

		<?php // echo $form->field($model, 'icons') ?>

		<?php // echo $form->field($model, 'header2') ?>

		<?php // echo $form->field($model, 'content2') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'seokeywords') ?>

		<?php // echo $form->field($model, 'seotitle') ?>

		<?php // echo $form->field($model, 'seoh1') ?>

		<?php // echo $form->field($model, 'seodescription') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
