<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "BrifController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class BrifController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Brif';
}
