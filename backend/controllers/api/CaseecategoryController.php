<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "CaseecategoryController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CaseecategoryController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Caseecategory';
}
