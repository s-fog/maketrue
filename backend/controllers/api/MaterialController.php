<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "MaterialController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class MaterialController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Material';
}
