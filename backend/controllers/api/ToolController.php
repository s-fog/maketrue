<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "ToolController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class ToolController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Tool';
}
