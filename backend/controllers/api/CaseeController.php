<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "CaseeController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CaseeController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Casee';
}
