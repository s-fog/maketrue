class How {
    constructor(root) {
        this.root = root;

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {
            circles: this.root.find('.how__circle'),
            line: this.root.find('.how__lineForward'),
            slider: this.root.find('.how__slider')
        }
    }

    _bindEvents() {
        this.nodes.circles.click((event) => {
            let index = $(event.currentTarget).index();
            let length = this.nodes.circles.length;

            this.removeCircleClasses();

            if (index == 1) {
                for (let i = 1; i<length; i++) {
                    this.nodes.circles.eq(i).addClass('how__circle_after');
                }

                this.main(event);
            } else if (index == length) {
                this.main(event);
            } else {
                for (let i = index; i<length; i++) {
                    this.nodes.circles.eq(i).addClass('how__circle_after');
                }

                this.main(event);
            }

            this.nodes.slider.trigger('to.owl.carousel', [index-1, 900]);
        });
    }

    _ready() {
        let startPosition = this.root.find('.how__circle_active').index() - 1;

        this.nodes.slider.owlCarousel({
            items: 1,
            nav: false,
            dots: false,
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false,
            startPosition: startPosition
        });
    }

    removeCircleClasses() {
        this.nodes.circles.removeClass('how__circle_active how__circle_after')
    }

    setLineWidth(left) {
        let lineLeft = left + 30;
        this.nodes.line.css('width', lineLeft + 'px')
    }

    main() {
        let left = parseInt($(event.currentTarget).css('left'));
        this.setLineWidth(left);
        $(event.currentTarget).addClass('how__circle_active')
    }
}

class Masonry {
    constructor(root) {
        this.root = root;

        this.animating = false;

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {
            catalog: $('.catalog__inner'),
            materials: $('.materials__inner'),
            catalogItems: $('.catalog__item'),
            catalogButton: $('.catalog .loadMore'),
            materialButton: $('.materials .loadMore'),
            materialItems: $('.materials__item'),
            categoriesCatalog: $('.catalog .catalog__categoriesItem'),
        }
    }

    _bindEvents() {
        this.nodes.materialButton.click((event) => {
            if (!this.animating) {
                let already = this.nodes.materialItems.length;
                let data = 'already=' + already;
                $(event.currentTarget).addClass('active');
                this.animating = true;

                $.post('/more/material', data, (response) => {
                    let res = $.parseJSON(response);
                    $(event.currentTarget).removeClass('active');
                    console.log(res);

                    if (parseInt(res.availableCount) >= parseInt(res.allCount)) {
                        $(event.currentTarget).hide();
                    } else {
                        $(event.currentTarget).show();
                    }

                    let $content = $(res.items);

                    this.nodes.materials.append($content).masonry('appended', $content);
                    setTimeout(() => {
                        this.nodes.materials.masonry('reloadItems').masonry();
                    }, 500);

                    setTimeout(() => {
                        this.nodes.materialItems = $('.materials__item');
                        this.animating = false;
                    }, 500);
                });
            }
        });

        this.nodes.catalogButton.click((event) => {
            if (!this.animating) {
                let already = this.nodes.catalogItems.length;
                let caseecategotyId = $('.catalog__categoriesItem.active').data('id');
                let data = 'already=' + already + '&caseecategotyId=' + caseecategotyId;
                $(event.currentTarget).addClass('active');
                this.animating = true;

                $.post('/more/catalog', data, (response) => {
                    let res = $.parseJSON(response);
                    $(event.currentTarget).removeClass('active');
                    console.log(res);

                    if (parseInt(res.availableCount) >= parseInt(res.allCount)) {
                        $(event.currentTarget).hide();
                    } else {
                        $(event.currentTarget).show();
                    }

                    let $content = $(res.items);

                    this.nodes.catalog.append($content).masonry('appended', $content);
                    setTimeout(() => {
                        this.nodes.catalog.masonry('reloadItems').masonry();
                    }, 500);

                    setTimeout(() => {
                        this.nodes.catalogItems = $('.catalog__item');
                        this.animating = false;
                    }, 500);
                });
            }
        });

        this.nodes.categoriesCatalog.click((event) => {
            if (!this.animating) {
                let caseecategotyId = $(event.currentTarget).data('id');
                let data = 'caseecategotyId=' + caseecategotyId;
                this.nodes.catalogButton.addClass('active');
                this.nodes.categoriesCatalog.removeClass('active');
                $(event.currentTarget).addClass('active');
                this.animating = true;

                $.post('/more/catalog', data, (response) => {
                    let res = $.parseJSON(response);
                    this.nodes.catalogButton.removeClass('active');
                    this.nodes.catalogItems.css('visibility', 'hidden');
                    console.log(res);

                    if (parseInt(res.availableCount) >= parseInt(res.allCount)) {
                        this.nodes.catalogButton.hide();
                    } else {
                        this.nodes.catalogButton.show();
                    }

                    $.when(
                        this.nodes.catalogItems.each((index, element) => {
                            this.nodes.catalog.masonry('remove', element).masonry('reloadItems').masonry();
                        })
                    ).then(() => {
                        let $content = $(res.items);

                        this.nodes.catalog.append($content).masonry('appended', $content);
                        setTimeout(() => {
                            this.nodes.catalog.masonry('reloadItems').masonry();
                        }, 500);

                        setTimeout(()=> {
                            this.nodes.catalogItems = $('.catalog__item');
                            this.animating = false;
                        }, 500)
                    });
                });
            }
        });
    }

    _ready() {
        setTimeout(() => {
            this.nodes.catalog.masonry({
                itemSelector: '.catalog__item',
                columnWidth: 355,
                gutter: 30,
                resize: false
            });

            this.nodes.materials.masonry({
                itemSelector: '.materials__item',
                columnWidth: 355,
                gutter: 30,
                resize: false
            });
        }, 200);
    }
}

class Application {
    constructor() {
        this._mainScripts();
        this._initClasses();
    }

    _mainScripts() {

        $('.openPopup').click(() => {
            $('.popupForm').addClass('active');
            $('body').addClass('hidden');
            return false;
        });

        $('.popupForm .close').click(() => {
            $('.popupForm').removeClass('active');
            $('body').removeClass('hidden');
            return false;
        });

        $('.sendForm').submit((event) => {
            event.preventDefault();
            var form = $(event.currentTarget);
            var fields = form.find('.input, textarea, select');
            var validate = true;

            fields.each((index, element) => {
                if (element.value == '') {
                    $(element).addClass('error');
                    validate = false;
                } else {
                    $(element).addClass('validateSuccess');
                    $(element).removeClass('error');
                }
            });

            if (!validate) {
                return false;
            }

            var formData = new FormData(form.get(0));
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "/mail/index");
            xhr.send(formData);

            xhr.upload.onprogress = () => {

            };

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4){
                    if (xhr.status == 200){
                        var response = xhr.responseText;

                        if (response == 'success') {
                            $.fancybox.close();
                            $('.popupForm').removeClass('active');

                            setTimeout(() => {
                                $.fancybox.open('<div class="success__wrapper">'+$('#success').html()+'</div>');
                            }, 200);

                            setTimeout(() => {
                                $.fancybox.close();
                            }, 5000);
                        } else {
                            alert('Ошибка');
                        }
                    } else {
                        console.log('error status');
                    }
                }
            };
        });

        $('.popupForm__overlay').css({
            'width': $(window).width() * 2,
            'height': $(window).height(),
            'left': '-' + ($(window).width() - 403)*2 + 'px',
        }).click(function() {
            $('.popupForm').removeClass('active');
            $('body').removeClass('hidden');
        });

        this._buttonPopup();

        $(window).load(() => {
            this._buttonPopup();
        });
    }

    _initClasses() {
        new How($('.how'));
        new Masonry();
    }

    _buttonPopup() {
        let windowWidth = $(window).width();
        let containerWidth = $('.mainHeader__inner').width();
        let right = (windowWidth - containerWidth) / 2 - 80;

        $('.mainHeader__popupOpen').css('right', right + 'px');
    }
}

(function () {
    new Application();
})();