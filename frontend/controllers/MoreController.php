<?php
namespace frontend\controllers;

use common\models\Casee;
use common\models\Material;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class MoreController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionCatalog() {
        $limit = 9;
        $where = [];

        if ($_POST['caseecategotyId'] != 0) {
            $where = ['caseecategory_id' => $_POST['caseecategotyId']];
        } else {
            $where = ['show_in_list' => 1];
        }

        if (isset($_POST['already']) && !empty($_POST['already'])) {
            $casees = Casee::find()
                ->where($where)
                ->limit($limit)
                ->orderBy(['order' => SORT_ASC])
                ->offset($_POST['already'])
                ->all();
            $allCaseesCount = count(Casee::find()
                ->orderBy(['order' => SORT_ASC])
                ->where($where)
                ->asArray()
                ->all()
            );
            $availableCaseesCount = count($casees) + $_POST['already'];
        } else {
            $casees = Casee::find()
                ->where($where)
                ->limit($limit)
                ->orderBy(['order' => SORT_ASC])
                ->all();
            $allCaseesCount = count(Casee::find()
                ->where($where)
                ->orderBy(['order' => SORT_ASC])
                ->asArray()
                ->all()
            );
            $availableCaseesCount = count($casees);
        }
        $result = [];

        foreach($casees as $model) {
            $item = str_replace('"', "'", trim($this->renderPartial('@frontend/views/casee/_item', ['model' => $model])));
            $item = str_replace(array("\r", "\n", "\r\n", "\t"), "", $item);
            $result[] = $item;
        }

        return '{"items": "'.implode('', $result).'", 
            "availableCount": "'.$availableCaseesCount.'",
            "allCount": "'.$allCaseesCount.'"}';
    }

    public function actionMaterial() {
        $materials = Material::find()
            ->limit(9)
            ->orderBy(['created_at' => SORT_DESC])
            ->offset($_POST['already'])
            ->all();
        $allMaterialsCount = count(Material::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()
            ->all()
        );
        $availableMaterialsCount = count($materials) + $_POST['already'];
        $result = [];

        foreach($materials as $model) {
            $item = str_replace('"', "'", trim($this->renderPartial('@frontend/views/material/_item', ['model' => $model])));
            $item = str_replace(array("\r", "\n", "\r\n", "\t"), "", $item);
            $result[] = $item;
        }

        return '{"items": "'.implode('', $result).'", 
            "availableCount": "'.$availableMaterialsCount.'",
            "allCount": "'.$allMaterialsCount.'"}';
    }
}
