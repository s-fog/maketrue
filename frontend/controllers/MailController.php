<?php

namespace frontend\controllers;

use Yii;
use yii\db\Query;

class MailController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if ($this->action->id == 'index')
        {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);

    }
    public function actionIndex() {
        if (!empty($_POST) && isset($_POST['type']) && !strlen($_POST['BC'])) {
            //var_dump($_FILES['file']);die();
            $labels = array(
                'name' => 'Имя',
                'phone' => 'Телефон',
                'email' => 'Эл. адрес',
                'date' => 'Дата',
                'material' => 'Материал',
                'glgel' => 'Покрыть картину объемным глянцевым гелем',
                'montazhPicture' => 'Монтаж картины',
                'designerCall' => 'Вызов дизайнера',
                'height' => 'Высота картины',
                'width' => 'Ширина картины',
                'period' => 'Срок изготовления картины',
                'url' => 'Ссылка на картинку',
            );

            $post = $_POST;
            $type = $post['type'];
            $msg = '';
            $to = 'deal@maketrue.pro';
            $headers = "Content-type: text/html; charset=\"utf-8\"\r\n";
            $headers .= "From: <deal@maketrue.pro>\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n";
            unset($post['type']);
            unset($post['url']);

            foreach($post as $name=>$value){
                $label = array_key_exists($name, $labels) ? $labels[$name] : $name;
                $value = htmlspecialchars($value);
                if(strlen($value)) {
                    if ($name == 'url') {
                        $msg .= "<p><b>$label</b>: <a href='$value'>$value</a></p>";
                    } else {
                        $msg .= "<p><b>$label</b>: $value</p>";
                    }
                }
            }

            $body = $msg;
            if (!empty($_FILES)) {
                $msg .= 'Плюс к этому письму приложен файл';
                $boundary = "--" . md5(uniqid(time()));
                $headers = "MIME-Version: 1.0;\r\n";
                $headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"";
                $body = "--$boundary\n";
                $body .= "Content-Type: text/html; charset=UTF-8\r\n";
                $body .= "Content-Transfer-Encoding: base64\r\n";
                $body .= "\r\n";
                $body .= chunk_split(base64_encode($msg));

                $i = 0;
                while($i < count($_FILES['file']['name'])) {
                    $file = $_FILES['file'];
                    $fp = fopen($file["tmp_name"][$i], "rb");

                    if (!$fp) {
                        echo "Cannot open file";
                        exit();
                    }

                    $data = fread($fp, filesize($file["tmp_name"][$i]));
                    fclose($fp);
                    $name = $file["name"][$i];

                    $body .= "\r\n\r\n--$boundary\r\n";
                    $body .= "Content-Type: " . $file["type"][$i] . "; name=\"$name\"\r\n";
                    $body .= "Content-Transfer-Encoding: base64 \r\n";
                    $body .= "Content-Disposition: attachment; filename=\"$name\"\r\n";
                    $body .= "\r\n";
                    $body .= chunk_split(base64_encode($data));

                    $i++;
                }

                $body .= "\r\n--$boundary--\r\n";
            }

            $emailSendError = false;
            foreach(explode(',', $to) as $email) {
                if(!mail($email, $type, $body, $headers)) {
                    $emailSendError = true;
                }
            }

            if ($emailSendError) {
                echo 'error';
            } else {
                echo 'success';
            }
        }
    }
}
