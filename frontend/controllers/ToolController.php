<?php
namespace frontend\controllers;

use common\models\Tool;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class ToolController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($alias)
    {
        $model = Tool::find()->where(['alias' => $alias])->one();

        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model
        ]);
    }
}
