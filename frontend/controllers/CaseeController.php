<?php
namespace frontend\controllers;

use common\models\Casee;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class CaseeController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($alias)
    {
        $model = Casee::find()->where(['alias' => $alias])->one();
        
        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model
        ]);
    }
    public function actionMore()
    {
        $this->enableCsrfValidation = false;
        $casees = Casee::find()
            ->limit(9)
            ->orderBy(['order' => SORT_ASC])
            ->offset($_POST['already'])
            ->all();
        $allCaseesCount = count(Casee::find()
            ->orderBy(['order' => SORT_ASC])
            ->asArray()
            ->all()
        );
        $availableCaseesCount = count($casees) + $_POST['already'];
        $result = [];

        foreach($casees as $model) {
            $item = str_replace('"', "'", trim($this->renderPartial('@frontend/views/casee/_item', ['model' => $model])));
            $item = str_replace(array("\r", "\n", "\r\n", "\t"), "", $item);
            $result[] = $item;
        }

        return '{"items": "'.implode('', $result).'", 
            "availableCount": "'.$availableCaseesCount.'",
            "allCount": "'.$allCaseesCount.'"}';
    }
}
