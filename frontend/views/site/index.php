<?php
use common\models\Brif;
use common\models\Tool;

$this->params['seotitle'] = 'Digital Рекламное агентство в Москве "MakeTrue"';
$this->params['seodescription'] = '';
$this->params['seokeywords'] = '';

?>
<div class="tools">
    <div class="container">
        <div class="tools__inner">
            <div class="tools__item tools__item_first">
                <div class="header withLine">Такой Digital<br>Вы еще не<br>Тестировали <span></span></div>
                <div class="tools__descr">Эффективный маркетинг на основе больших данных. </div>
                <a href="/cases" class="button">
                    <span class="button__text">ЛЕНЬ ЧИТАТЬ - ГДЕ КЕЙСЫ?</span>
                    <span class="button__s"></span>
                </a>
            </div>
            <?php
            $tools = Tool::items('onmain');
            foreach($tools as $model) {
                echo $this->render('@frontend/views/tool/_item', ['model' => $model]);
            }
            ?>
            <div class="tools__item tools__item_last">
                <br>
                <div class="header withLine">Маркетинг, основанный<br>на больших<br>данных<span></span></div>
                <div class="tools__descr">Сегменты аудитория<br>Управление данными<br>Обогащение данных<br>Look-alike модели</div>
            </div>
        </div>
    </div>
</div>


<!--
<div class="clients">
    <div class="container">
        <div class="clients__inner">
            <div class="clients__left">
                <div class="header withLine">Любимые клиенты <span></span></div>
                <div class="clients__text">Раскрученные и известные бренды – это хорошо. С ними просто и легко работать. Хотя, после совместного сотрудничества таковыми становятся все наши клиенты. Долгосрочные контракты, основанные на эффективных решениях – для нас обыденное дело.</div>
                <a href="/cases" class="button">
                    <span class="button__text">ВСЕ ДОВОЛЬНЫЕ КЛИЕНТЫ</span>
                    <span class="button__s"></span>
                </a>
            </div>
            <div class="clients__right">
                <a href="/cases/marketing-dla-gipermarketa-russkih-podarkov" class="clients__image"><img src="/uploads/cases/rus.png" alt=""></a>
                <a href="/cases/prodaza-nedvizimosti-biznes-centr-a-klassa" class="clients__image"><img src="/uploads/cases/olimpik.png" alt=""></a>
                <a href="/cases/venum-diler-odezdy-i-ekipirovki-v-rf" class="clients__image"><img src="/uploads/cases/venum.png" alt=""></a>
                <a href="/cases/prodaza-avtomobilej-kiabrmoskovskaa-oblast" class="clients__image"><img src="/uploads/cases/kia.png" alt=""></a>
            </div>
        </div>
    </div>
</div>
-->


<div class="mountains">
    <div class="container">
        <div class="mountains__inner">
            <div class="mountains__item">
                <div class="mountains__image" style="background-image: url(/img/mountain1.png);"></div>
                <div class="mountains__info">
                    <div class="mountains__header">КОНВЕРСИЯ</div>
                    <div class="mountains__text">Работаем лишь с теми проектами, KPI которых были согласованы.
                        Если нельзя отследить эффективность - зачем работать?</div>
                </div>
            </div>
            <div class="mountains__item">
                <div class="mountains__image" style="background-image: url(/img/mountain2.png);"></div>
                <div class="mountains__info">
                    <div class="mountains__header">АВТОМАТИКА</div>
                    <div class="mountains__text">Пишем роботов для рутинных задач. За счет этого, мы в десятки раз выигрываем в КПД по ресурсоемким задачам.</div>
                </div>
            </div>
            <div class="mountains__item">
                <div class="mountains__image" style="background-image: url(/img/mountain3.png);"></div>
                <div class="mountains__info">
                    <div class="mountains__header">СТРАТЕГИЯ</div>
                    <div class="mountains__text">Обязательным шагом перед началом работ является мадиаплан и стратегия будущей рекламной кампании.</div>
                </div>
            </div>
            <div class="mountains__item">
                <div class="mountains__image" style="background-image: url(/img/mountain4.png);"></div>
                <div class="mountains__info">
                    <div class="mountains__header">АНАЛИТИКА</div>
                    <div class="mountains__text">Анализ данных выполняется
                        в нашем программном обеспечении. Сегменты в любом разрезе, под любым углом.</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="innovate">
    <div class="container">
        <div class="header text-center">Мы изобретаем</div>
        <br>
        <div class="text text-center">Все то, чего нет, но очень хочется. В случае, когда для полноценной <br>работы над проектом нет необходимого инструмента, он разрабатывается.<br>Некоторые решения создаются исключительно под нужды и комфорт заказчика.</div>
        <br>
        <br>
        <div class="innovate__bigImage"></div>
        <?=$this->render('@frontend/views/blocks/innovate')?>
    </div>
</div>

<div class="brifs">
    <div class="container">
        <div class="header text-center">Брифы | Решения</div>
        <br>
        <br>
		<!--
        <div class="text text-center">Мы вас уважаем. Ценим ваше время. Стоимость каждого проекта складывается из<br>десятков переменых, начиная возрастом бренда и заканчивая компетентностью<br>вашего сотрудника, отвечающего за продажи. </div>
        <br>-->
        <div class="text text-center">Уделите пожалуйста 5 минут времени и дайте ответы на вопросы брифа.<br>Взамен, мы приложим все усилия и грамотно проанализируем ваш бизнес. Сформируем<br>понятное комерческое предложение и пропишем как очевидные вещи, так и тонкости,<br>свойственные некоторым нишам.</div>
        <br><br><br><br>
        <div class="tools__inner">
            <?php
            $brifs = Brif::items();
            foreach($brifs as $model) {
                echo $this->render('@frontend/views/brif/_item', ['model' => $model]);
            }
            ?>
        </div>
    </div>
</div>

<?=$this->render('@frontend/views/blocks/cases')?>
<?php /*
<div class="how">
    <div class="container">
        <!--<div class="paddingLeft margin content">
            <div class="header withLine">MakeTrue - Как мы выбрались на рынок <span></span></div>
            <div class="text">Коллектив MakeTrue - выходцы из крупнейших рекламных агентств Москвы и Санкт-Петербурга. Несколько лет мы существовали, обслуживая крупных клиентов других РА, скрываясь в тени. Накопив сил и денег, вышли на просторный рынок Digital в РФ, большинство обывателей которого уже хорошо нам знакомы.</div>
            <br>
            <div class="text"><b>Мы с зубами!</b><br>
                За 5 лет мы попробовали десятки инструментов, вложив в тесты десятки миллионов рублей. Поэтому, если мы говорим, что какая-либо сторона хромает в вашем маркетинге - будьте добры, прислушайтесь!</div>
            <br>
        </div>-->
        <div class="how__line">
            <div class="how__lineForward" style="width: 68%;"></div>
            <div class="how__circle" style="left: 5%;">2014<br><span class="how__month">Сентябрь</span></div>
            <div class="how__circle" style="left: 25%;">2016<br><span class="how__month">Октябрь</span></div>
            <div class="how__circle how__circle_active" style="left: 65%;">2017<br><span class="how__month">Февраль</span></div>
            <div class="how__circle how__circle_after" style="left: 75%;">2017<br><span class="how__month">Май</span></div>
            <div class="how__circle how__circle_after" style="left: 90%;">2017<br><span class="how__month">Июль</span></div>
        </div>
        <div class="how__slider owl-carousel">
            <div class="how__sliderItem">
                <div class="how__sliderImage" style="background-image: url(/img/flag.png);"></div>
                <div class="how__sliderInfo">
                    <div class="text">Сентябрь, 2014</div>
                    <div class="how__sliderHeader">Первый шаг</div>
                    <div class="text">Знаменательный год, когда определилось будущее РА MakeTrue. Зародилась новая команда, готовая к грандиозным победам и непокоренным вершинам. В этот момент мы уже строили планы по открытию собственного рекламного агентства, мечтали о зарубежном сотрудничестве и вскоре его получили.</div>
                </div>
            </div>
            <div class="how__sliderItem">
                <div class="how__sliderImage" style="background-image: url(/img/flag.png);"></div>
                <div class="how__sliderInfo">
                    <div class="text">Октябрь, 2016</div>
                    <div class="how__sliderHeader">На пути к цели</div>
                    <div class="text">Реализованные свыше 140-проектов дали возможность приступить к созданию собственной многофункциональной системы распределения и хранения данных (Data Management Platform).  Именно благодаря накопленной с того момента Big Data, мы успешно реализуем SEO-проекты сейчас.</div>
                </div>
            </div>
            <div class="how__sliderItem">
                <div class="how__sliderImage" style="background-image: url(/img/flag.png);"></div>
                <div class="how__sliderInfo">
                    <div class="text">Февраль, 2017</div>
                    <div class="how__sliderHeader">Новый бренд MakeTrue</div>
                    <div class="text">Ушли с работ и собрались вместе! Коллектив MakeTrue - выходцы из крупнейших рекламных агентств Москвы и Санкт-Петербурга. Несколько лет мы существовали, обслуживая крупных клиентов других РА, скрываясь в тени. Накопив сил и денег, вышли на просторный рынок Digital в РФ, большинство обывателей которого уже хорошо нам знакомы.</div>
                </div>
            </div>
            <div class="how__sliderItem">
                <div class="how__sliderImage" style="background-image: url(/img/flag.png);"></div>
                <div class="how__sliderInfo">
                    <div class="text">Май, 2017</div>
                    <div class="how__sliderHeader">Масштабы активно расширяются</div>
                    <div class="text">Пришло время расширить географию деятельности: сотрудничество с зарубежными компаниями по реализации рекламных проектов приносит свои плоды. На тот момент мы работали по 32 странам мира, создавая проекты с таргетингом на 7 языках. Поставленные однажды планы стали реальными, поэтому мы со спокойной душой приступили к реализации новых проектов.</div>
                </div>
            </div>
            <div class="how__sliderItem">
                <div class="how__sliderImage" style="background-image: url(/img/flag.png);"></div>
                <div class="how__sliderInfo">
                    <div class="text">Июль, 2017</div>
                    <div class="how__sliderHeader">Новый день – новое начало</div>
                    <div class="text">Мы всегда находимся в процессе создания чего-то нового. Каждый день приносить свои открытия, поэтому мы никогда не упускаем шанс создать нечто гениальное и полезное. Развитие нашей компании обрело еще одну веху при помощи инструмента собственной разработки, о котором вы вскоре узнаете.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<? */ ?>

<div class="waves beforeFooter">
    <div class="container">
        <div class="paddingLeft">
            <div class="header withLine">Stream | Данные, идеи и размышления <span></span></div>
            <div class="text">Мы любим делиться собственным опытом и знаниями с миром. Если вы с нами на одной волне, вам будет интересно почитать наши публикации. Открывайте каждый день для себя что-то новое вместе с нами!</div>
            <br>
            <br>
            <br>
        </div>
        <?=$this->render('@frontend/views/blocks/materials')?>
    </div>
</div>


