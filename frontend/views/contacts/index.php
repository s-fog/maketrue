<?php
$this->params['seotitle'] = 'Контакты и адрес компании MakeTrue';
$this->params['seodescription'] = '';
$this->params['seokeywords'] = '';
$this->params['seoh1'] = 'Контакты MakeTrue';
?>
    <div class="contentWithImage case">
        <div class="container">
            <div class="contentWithImage__inner">
                <div class="contentWithImage__left">
                    <img src="/uploads/pic/pic-segment.png" alt="">
                </div>
                <div class="contentWithImage__right">
                    <h1 class="header withLine"><?=$this->params['seoh1']?> <span></span></h1>
                    <p>Мы открыты к новым деловым партнерствам. Звоните и пишите нам - главное, чтобы мы оказились полезными друг другу. Разве не в этом заключается успешный обмен опытом и его применение в вашей конкретной нише?</p>
                    <p>Нам нравится живое общение, поэтому будьте готовы к тому, что мы назначим встречу на ближайшее удобное Вам время. Сухое общение по телефону либо электронной почте не наш конёк.</p>
                </div>
            </div>
            <div class="contacts">
                <div class="contacts__icons">
                    <a href="" class="contacts__icon">
                        <span class="contacts__iconImage"><img src="/img/ci1.png" alt=""></span>
                        <span class="contacts__iconTop">Россия Москва</span>
                        <span class="contacts__iconBottom">Спартаковская площадь 14С2<br>Офис 2202</span>
                    </a>
                    <a href="mailto:s-fog@yandex.ru" class="contacts__icon">
                        <span class="contacts__iconImage"><img src="/img/ci2.png" alt=""></span>
                        <span class="contacts__iconTop">deal@maketrue.pro</span>
                        <span class="contacts__iconBottom">Электронная <br>почта</span>
                    </a>
                    <a href="tel:84993806518" class="contacts__icon">
                        <span class="contacts__iconImage"><img src="/img/ci3.png" alt=""></span>
                        <span class="contacts__iconTop">8 499 380-65-18</span>
                        <span class="contacts__iconBottom">Многоканальный <br>телефон</span>
                    </a>
                </div>
                <br>
                <div class="contactsColumns margin">
                    <div class="contactsColumns__left">
                        <div class="header withLine">Задача <span></span></div>
                        <p class="text">Просим излагать задачу точно и описывать все тонкости ниши. Мы не компетентны в вашем продукте, поэтому подводные камни знаете только Вы.</p>
                    </div>
                    <div class="contactsColumns__right">
                        <div class="header withLine">Решение <span></span></div>
                        <p class="text">Чем больше вводных Вы сообщите, тем более точный инструмент мы сможем подобрать и настроить. В зависимости от динамики рынка, предложения, сезонности и конкуренции, эффективность инструментов будет сильно варьироваться. Все это крайне важно учитывать при формировании медиаплана.</p>
                    </div>
                </div>
            </div>
            <div id="map">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A70b6e411ffda231a95fbcfb30cd660ec4ff8c382befaf041020d8b4918526168&amp;width=100%25&amp;height=470&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
            <div class="text-center margin">
                <div class="button4 openPopup">
                    <span class="button4__text">Задайте вопрос</span>
                    <span class="button4__s"></span>
                </div>
            </div>
        </div>
    </div>

