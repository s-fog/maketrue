<?php 
use yii\helpers\Url;
?>
<a href="<?=Url::to(['casee/view', 'alias' => $model->alias])?>" class="catalog__item">
    <img src="<?=$model->previewimage?>" class="catalog__itemImage">
    <span class="catalog__itemHeader"><?=$model->name?></span>
    <span class="catalog__itemText"><?=$model->introtext?></span>
                    <span class="text-center" style="display: block;">
                        <span class="button3">
                            <span class="button3__text">Посетить &rarr;</span>
                            <span class="button3__s"></span>
                        </span>
                    </span>
</a>