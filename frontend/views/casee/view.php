<?php

use yii\helpers\Url;

$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
$this->params['name'] = $model->name;

?>
<div class="contentWithImage without-padding-bottom">
    <div class="container">
        <div class="contentWithImage__inner">
            <!--<div class="contentWithImage__left">
                <img src="<?=$model->image?>" alt="">
            </div>-->
            <div class="contentWithImage__right margin-left-null">
                <h1 class="header withLine">
                    <?=(!empty($model->seoh1))? $model->seoh1 : $model->name?>
                    <span></span>
                </h1>
                <?=$model->content?>
            </div>
        </div>
    </div>
</div>
<div class="case">
    <div class="container">
        <div class="case__inner">
            <div class="case__left">
                <?php if (!empty($model->inimage1)) { ?>
                    <div class="case__image">
                        <img src="<?=$model->inimage1?>" alt="">
                    </div>
                <?php } ?>
                <?php if (!empty($model->inimage2)) { ?>
                    <div class="case__image">
                        <img src="<?=$model->inimage2?>" alt="">
                    </div>
                <?php } ?>
            </div>
            <div class="case__right">
                <?php if (!empty($model->task)) { ?>
                    <div class="case__part">
                        <div class="header withLine">Задача <span></span></div>
                        <div class="case__partContent content"><?=$model->task?></div>
                    </div>
                <?php } ?>
                <?php if (!empty($model->solution)) { ?>
                    <div class="case__part">
                        <div class="header withLine">Решение <span></span></div>
                        <div class="case__partContent content"><?=$model->solution?></div>
                    </div>
                <?php } ?>
                <?php if (!empty($model->analitics)) { ?>
                    <div class="case__part">
                        <div class="header withLine">Аналитика <span></span></div>
                        <div class="case__partContent content"><?=$model->analitics?></div>
                    </div>
                <?php } ?>
                <?php if (!empty($model->icons)) { ?>
                    <div class="case__part">
                        <div class="case__icons">
                            <?=$model->icons?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php if (!empty($model->header2) && !empty($model->content2)) { ?>
            <div class="content margin">
                <div class="header withLine"><?=$model->header2?> <span></span></div>
                <?=$model->content2?>
            </div>
        <?php } ?>
        <a href="<?=Url::to(['casee/index'])?>" class="button3">
            <span class="button3__text">Назад</span>
            <span class="button3__s"></span>
        </a>
    </div>
</div>
