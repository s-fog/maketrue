<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\models\Brif;
use common\models\Tool;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?=(isset($this->params['seotitle']) && !empty($this->params['seotitle']))? $this->params['seotitle'] : $this->params['name']?></title>
    <?=(isset($this->params['seodescription']) && !empty($this->params['seodescription']))? '<meta name="description" content="'.$this->params['seodescription'].'">' : ''?>
    <?=(isset($this->params['seokeywords']) && !empty($this->params['seokeywords']))? '<meta name="keywords" content="'.$this->params['seokeywords'].'">' : ''?>
    <?php $this->head() ?>
	<script>
		var path = window.location.pathname; 
		if(path.indexOf('/seo-poiskovaa-optimizacia') >= 0 ||
			path.indexOf('/blog') >= 0 ||
			path.indexOf('/street-retail') >= 0
		) {     
		console.log('yes'); 
		}   
	</script>
	<script type="text/javascript" src="https://test2.dmp.one/sync" async charset="UTF-8"></script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="mainHeader">
    <div class="container">
        <div class="mainHeader__inner">
            <a href="/" class="logo">
                <img src="/img/logo-eye.png" alt="">
                <span class="logo__top">MakeTrue</span>
                <span class="logo__bottom">SEO, Digital Marketing</span>
            </a>
            <ul class="menu">
                <li><a href="/" class="menu__link">Домой</a></li>
                <li>
                    <ul class="menu__sub">
                        <li class="menu__subHeader">Инструменты</li>
                        <li class="menu__subDescr">Ключевые навыки и компетенции команды MakeTrue</li>
                        <?php
                        $tools = Tool::items();
                        foreach($tools as $model) { ?>
                            <li class="menu__subItem">
                                <a href="<?=Url::to(['tool/view', 'alias' => $model->alias])?>"
                                   class="menu__subLink">
                                    <?=(!empty($model->menutitle))? $model->menutitle : $model->name?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <span class="menu__link"><span>Инструменты</span></span>
                </li>
                <li>
                    <ul class="menu__sub">
                        <li class="menu__subHeader">Брифы</li>
                        <li class="menu__subDescr">Перед созданием предложения, нам потребуется бриф</li>
                        <?php
                        $brifs = Brif::items();
                        foreach($brifs as $model) { ?>
                            <li class="menu__subItem">
                                <a href="<?=Url::to(['brif/view', 'alias' => $model->alias])?>"
                                   class="menu__subLink">
                                    <?=(!empty($model->menutitle))? $model->menutitle : $model->name?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <span class="menu__link"><span>Брифы</span></span>
                </li>
                <li><a href="/cases" class="menu__link">Кейсы</a></li>
                <li><a href="/blog" class="menu__link">Материалы</a></li>
                <li><a href="/contacts" class="menu__link">Контакты</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="mainHeader__popupOpen openPopup">
    <div class="mainHeader__popupOpenInner">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

<?=$content?>

<div class="mainFooter">
    <div class="container">
        <div class="mainFooter__inner">
            <div class="mainFooter__left">
                <div class="mainFooter__header header withLine">Мы благодарны вам <span></span></div>
                <div class="mainFooter__text">Мы ценим ваше время и благодарны за интерес к нашей компании. Мы считаем, что нашли свою нишу на рынке Digital рекламы России и Европы. Точно знаем кто наш клиент, что дает возможность фильтровать проекты, запуская в работу лишь самые интересные.
                </div>
                <div class="socials">
                    <a href="https://www.facebook.com/maketrue.pro" class="socials__item" target="_blank">
                        <img src="/img/circle-facebook.svg" alt="">
                    </a>
                    <a href="https://vk.com/maketrue" class="socials__item" target="_blank">
                        <img src="/img/vk.svg" alt="">
                    </a>
                    <!--<a href="https://aboutme.google.com/b/108753817228705630526/" class="socials__item" target="_blank">
                        <img src="/img/google.svg" alt="">
                    </a>-->
                    <a href="https://www.instagram.com/maketrue.pro/" class="socials__item" target="_blank">
                        <img src="/img/instagram.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="mainFooter__right">
                <div class="mainFooter__header header withLine">Компания Меню <span></span></div>
                <ul class="mainFooter__menu">
                    <li>
                        <a href="/">Домой</a>
                    </li>
                    <li>
                        <a href="/cases">Кейсы</a>
                    </li>
                    <li>
                        <a href="/blog">Материалы</a>
                    </li>
                    <li>
                        <a href="/contacts">Контакты</a>
                </ul>
            </div>
        </div>
        <div class="mainFooter__icons">
            <a href="tel:84993806518" class="mainFooter__icon">
                <span class="mainFooter__iconImage"><svg enable-background="new 0 0 64 64" version="1.1" viewBox="0 0 64 64" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" class=" drawsvg-initialized "><path d="  M45.1,44.2C42.9,42,39.6,40,37,42.6c-1.8,1.8-2.6,3.9-2.6,3.9s-4.3,2.3-11.7-5.2s-5.2-11.7-5.2-11.7s2.1-0.8,3.9-2.6  c2.6-2.6,0.6-5.9-1.7-8.1c-2.7-2.7-6.2-4.9-8.2-2.9c-3.7,3.7-4.4,8.4-4.4,8.4S9,35.5,18.7,45.3s20.9,11.6,20.9,11.6s4.7-0.7,8.4-4.4  C50,50.4,47.8,46.9,45.1,44.2z" fill="none" stroke="#fcb03b" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 141.862, 141.862; stroke-dashoffset: 0;"></path><path d="  M18.4,12.2C22.2,9.5,26.9,8,32,8c13.3,0,24,10.8,24,24c0,4-1.3,9-4.4,12.2" fill="none" stroke="#fcb03b" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 65.4371, 65.4371; stroke-dashoffset: 0;"></path><path d="  M27.3,55.6c-9.8-1.9-17.5-9.8-19.1-19.7" fill="none" stroke="#fcb03b" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 29.2031, 29.2031; stroke-dashoffset: 0;"></path><path d="  M30,21c0,0,4.4,0,5.2,0c1.2,0,1.8,0.2,1.8,1.1s0,0.7,0,1.3c0,0.6,0,1.4-1.6,2.5c-2.3,1.6-5.6,3.8-5.6,5.1c0,1.6,0.7,2,1.8,2  s5.3,0,5.3,0" fill="none" stroke="#fcb03b" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 28.1923, 28.1923; stroke-dashoffset: 0;"></path><path d="  M40,21c0,0,0,2.8,0,3.8S39.9,27,41.5,27c1.6,0,4.5,0,4.5,0v-6.1V33" fill="none" stroke="#fcb03b" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 29.6307, 29.6307; stroke-dashoffset: 0;"></path></svg></span>
                <span class="mainFooter__iconTop">8 499 380-65-18</span>
                <span class="mainFooter__iconBottom">Многоканальный телефон</span>
            </a>
            <a href="mailto:deal@maketrue.pro" class="mainFooter__icon">
                <span class="mainFooter__iconImage"><svg enable-background="new 0 0 64 64" version="1.1" viewBox="0 0 64 64" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" class=" drawsvg-initialized "><polyline fill="none" points="  54,17 32,36 10,17 " stroke="#f15b26" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"></polyline><line fill="none" stroke="#f15b26" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" x1="10.9" x2="26" y1="48" y2="36"></line><path d="  M32.7,49H13c-2.2,0-4-1.8-4-4V19c0-2.2,1.8-4,4-4h38c2.2,0,4,1.8,4,4v15.5" fill="none" stroke="#f15b26" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 118.04, 118.04; stroke-dashoffset: 0;"></path><circle cx="44.9" cy="43.1" fill="none" r="10.1" stroke="#f15b26" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"></circle><path d="  M44,41.4c0,0-1.3,3.4-0.9,5.1c0.4,1.7,2.6,2.1,3.7,1.1" fill="none" stroke="#f15b26" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 9.82995, 9.82995; stroke-dashoffset: 0;"></path><g><circle cx="45.4" cy="38.3" fill="#DCE9EE" r="0.9"></circle><path d="M45.4,37.3c-0.5,0-0.9,0.4-0.9,0.9c0,0.5,0.4,0.9,0.9,0.9s0.9-0.4,0.9-0.9C46.4,37.8,46,37.3,45.4,37.3   L45.4,37.3z" fill="#f15b26" style="stroke-dasharray: 5.69581, 5.69581; stroke-dashoffset: 0;"></path></g></svg></span>
                <span class="mainFooter__iconTop">deal@maketrue.pro</span>
                <span class="mainFooter__iconBottom">Электронная почта</span>
            </a>
            <a href="#" class="mainFooter__icon">
                <span class="mainFooter__iconImage"><svg enable-background="new 0 0 64 64" version="1.1" viewBox="0 0 64 64" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" class=" drawsvg-initialized "><polygon fill="none" points="  38.7,36.4 56,32 38.7,27.6 42,22 36.4,25.3 32,8 27.6,25.3 22,22 25.3,27.6 8,32 25.3,36.4 22,42 27.6,38.7 32,56 36.4,38.7 42,42   " stroke="#3cb878" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"></polygon><circle cx="32" cy="32" fill="none" r="4" stroke="#3cb878" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"></circle><path d="  M26.1,53.2c-7.9-2.2-13.9-8.6-15.6-16.7" fill="none" stroke="#3cb878" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 24.0354, 24.0354; stroke-dashoffset: 0;"></path><path d="  M53.5,36.9c-1.8,8.1-8.2,14.6-16.3,16.5" fill="none" stroke="#3cb878" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 24.4411, 24.4411; stroke-dashoffset: 0;"></path><path d="  M36.9,10.5c8.2,1.9,14.7,8.3,16.6,16.6" fill="none" stroke="#3cb878" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 24.7444, 24.7444; stroke-dashoffset: 0;"></path><path d="  M10.5,27.1c1.9-8.2,8.3-14.6,16.4-16.5" fill="none" stroke="#3cb878" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" style="stroke-dasharray: 24.505, 24.505; stroke-dashoffset: 0;"></path></svg></span>
                <span class="mainFooter__iconTop">Россия Москва</span>
                <span class="mainFooter__iconBottom">Спартаковская площадь 14С2 офис 2202</span>
            </a>
        </div>
    </div>
    <div class="mainFooter__bottom">
        <div class="container">
            <span>
                        Copyright © 2019 MakeTrue |
                        Designed by MakeTrue</a>
                    </span>
        </div>
    </div>
</div>

<form class="popupForm sendForm">
    <div class="popupForm__outer">
        <div class="popupForm__inner">
            <a href="/" class="logo">
                <img src="/img/logo-eye.png" alt="">
                <span class="logo__top">MakeTrue</span>
                <span class="logo__bottom">SEO, Digital Marketing</span>
            </a>
            <p class="text contacts-text-head">Заполните форму контактными данными и мы свяжемся с вами так быстро, как только сможем. Много работы, правда;)
            </p>
            <div class="popupForm__header">Обратная связь</div>
            <div class="form-group">
                <input type="text" name="name" placeholder="Имя" class="input">
            </div>
            <div class="form-group">
                <input type="text" name="phone" placeholder="Телефон" class="input">
            </div>
            <div class="form-group">
                <input type="text" name="email" placeholder="Почта" class="input">
            </div>
            <button type="submit" class="button">
                <span class="button__text">Отправить</span>
                <span class="button__s"></span>
            </button>
            <aside id="seosight_contacts-2" class="widget w-contacts">
                <h4 class="heading-title">Мы на связи</h4>
                <p class="contacts-text">Готовы ответить на вопросы по нашим маркетинговым продуктам. Для запроса КП, заполните <a href="/brif">бриф</a> пожалуйста.</p>
                <div class="contacts__icons">
                    <a href="" class="contacts__icon">
                        <span class="contacts__iconImage"><img src="/img/ci1.png" alt=""></span>
                        <span class="contacts__iconTop">Россия Москва</span>
                        <span class="contacts__iconBottom">Южнопортовая 7С1 <br>Офис 310</span>
                    </a>
                    <a href="mailto:deal@maketrue.pro" class="contacts__icon">
                        <span class="contacts__iconImage"><img src="/img/ci2.png" alt=""></span>
                        <span class="contacts__iconTop">deal@maketrue.pro</span>
                        <span class="contacts__iconBottom">Электронная <br>почта</span>
                    </a>
                    <a href="tel:84993806518" class="contacts__icon">
                        <span class="contacts__iconImage"><img src="/img/ci3.png" alt=""></span>
                        <span class="contacts__iconTop">8 499 380-65-18</span>
                        <span class="contacts__iconBottom">Многоканальный <br>телефон</span>
                    </a>
                </div>
            </aside>
        </div>
    </div>
    <div class="close">
        <div class="close__inner">
            <span></span>
            <span></span>
        </div>
    </div>
    <input type="hidden" name="type" value="Обратный звонок с сайта Maketrue">
    <input type="text" name="BC" class="BC">
    <input type="hidden" name="url" value="<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>">
    <div class="popupForm__overlay"></div>
</form>

<div class="success" id="success">
    <div class="logo">
        <img src="/img/logo-eye.png" alt="">
        <span class="logo__top">MakeTrue</span>
        <span class="logo__bottom">SEO, Digital, Marketing</span>
    </div>
    <div class="success__text">Мы получили Ваше сообщение и старгетируем на Вас наше внимение в кратчайшие сроки. Благодарим за обратную связь</div>
</div>

<?php $this->endBody() ?>

<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter40840589 = new Ya.Metrika({ id:40840589, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script src="//code.jivosite.com/widget/OSACn2qJYj" async></script>
<!-- {/literal} END JIVOSITE CODE -->

<!-- BEGIN LEADGENIC CODE {literal}
<noindex>
<script type="text/javascript" src="https://gate.leadgenic.ru/getscript?site=5920806b0cf2f772c5dc68b1"></script>
</noindex>
<!-- {/literal} END LEADGENIC CODE -->

</body>
</html>
<?php $this->endPage() ?>
