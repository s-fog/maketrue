<div class="innovate__inner">
    <div class="innovate__item" style="background-image: url(/img/innovate1.png);">
        <div class="innovate__header">Data Management
            Управление данными</div>
        <div class="innovate__text">Взглянув на трафик вашего ресурса, мы точно подберем всех, кому может быть  интересно ваше предложение.</div>
    </div>
    <div class="innovate__item" style="background-image: url(/img/innovate2.png);">
        <div class="innovate__header">Предоставим данные
            о ваших клиентах</div>
        <div class="innovate__text">Платформа управления данными содержит более 200 миллионов профилей посетителей и их устройств.</div>
    </div>
    <div class="innovate__item" style="background-image: url(/img/innovate3.png);">
        <div class="innovate__header">Web-Mobile Push
            Не теряйте клиентов</div>
        <div class="innovate__text">С технологией Push вы всегда на связи со своими клиентами
            и способны мгновенно донести нужную информацию!</div>
    </div>
</div>