<?php
use common\models\Material;
?>
        <div class="materials">
            <div class="materials__inner">
                <?php
                $materials = Material::find()
                    ->limit(9)
                    ->orderBy(['created_at' => SORT_DESC])
                    ->all();
                foreach($materials as $model) {
                    echo $this->render('@frontend/views/material/_item', ['model' => $model]);
                }
                ?>
            </div>
            <?php
            if (count($materials) > 9) {
            ?>
                <div class="loadMore">
                    <span>
                        <img src="/img/load-more-line.png">
                    </span>
                    <div class="loadMore__text">Загрузить еще</div>
                </div>
            <?php } ?>
        </div>
        <br>
        <br>
        <div class="paddingLeft margin content">
                    <div class="header withLine">Залог успеха каждого проекта<span></span></div>
                    <div class="text">Мы разработали собственную систему сотрудничества по брифам для получения максимальной эффективности поставленных задач. Поручая нам исполнение проекта, вы получаете отчет по основным каналам маркетинга, дающий полное представление о вашем бизнесе на Digital рынке. </div>
                    <br>
                    <div class="text"><b>Мы благодарны вам!</b><br>
                        Мы ценим ваше время и благодарны за интерес к нашей компании. Мы считаем, что нашли свою нишу на рынке Digital рекламы России и Европы. Мы точно знаем, кто наш клиент, что дает возможность запускать в работу лишь самые интересные проекты.</div>
                    <br>
                </div>