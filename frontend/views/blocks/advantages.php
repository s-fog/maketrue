<div class="advantages">
    <div class="container">
        <div class="advantages__inner">
            <div class="advantages__item">
                <!--<div class="advantages__icon" style="background-image: url(/img/487129304eca93e3646dd0c7dd441bf5.png);"></div>-->
                <div class="advantages__digit">83 %</div>
                <div class="advantages__text">Organic Traffic - Increase</div>
            </div>
            <div class="advantages__item">
                <!--<div class="advantages__icon" style="background-image: url(/img/no-translate-detected_318-64411.jpg);"></div>-->
                <div class="advantages__digit">83 %</div>
                <div class="advantages__text">Bounce Rate - Decrease</div>
            </div>
            <div class="advantages__item">
                <!--<div class="advantages__icon" style="background-image: url(/img/rankings-512.png);"></div>-->
                <div class="advantages__digit">83 %</div>
                <div class="advantages__text">Average Visit Duration - Increase</div>
            </div>
            <div class="advantages__item">
                <!--<div class="advantages__icon" style="background-image: url(/img/SEO_Analytic-512.png);"></div>-->
                <div class="advantages__digit">83 %</div>
                <div class="advantages__text">Pages Per Session - Increase</div>
            </div>
        </div>
    </div>
</div>