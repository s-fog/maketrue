<?php
use common\models\Casee;
use common\models\Caseecategory;

?>
<div class="catalog">
    <div class="container">
        <div class="header text-center withLine withLine_center">Кейсы и решения от MakeTrue <span></span></div>
        <div class="text text-center">Команда MakeTrue собрала для вас лучшие модели и инструменты маркетинга.<br>Выбор оптимального инструмента станет выгодным решением для вашего бизнеса.</div>
        <br>
        <div class="catalog__categories">
            <div class="catalog__categoriesItem active" data-id="0">Все проекты</div>
            <?php foreach(Caseecategory::find()->all() as $model) {
                echo '<div class="catalog__categoriesItem" data-id="'.$model->id.'">'.$model->name.'</div>';
            } ?>
        </div>
        <div class="catalog__inner">
            <?php $cases = Casee::find()
                ->where(['show_in_list' => 1])
                ->limit(9)
                ->orderBy(['order' => SORT_ASC])
                ->all(); ?>
            <?php foreach ($cases as $model) {
                echo $this->render('@frontend/views/casee/_item', ['model' => $model]);
            } ?>
        </div>
        <div class="loadMore"<?=(count($cases) != 9)? ' style="display: none;"' : ''?>>
                <span>
                    <img src="/img/load-more-line.png">
                </span>
            <div class="loadMore__text">Загрузить еще</div>
        </div>
        <br>
        <!--
		<div class="paddingLeft margin content">
            <div class="header withLine">MakeTrue – Рождение единой команды<span></span></div>
            <div class="text">Команда MakeTrue – это бывшие сотрудники крупнейших рекламных агентств Москвы и Санкт-Петербурга. Мы собирали опыт и знания, чтобы вскоре воплотить самые успешные и эксцентричные проекты в вашем бизнесе. Пришло время выйти из тени и покорить многогранный Digital рынок России, с которым мы разговариваем уже на «ты».</div>
            <br>
            <div class="text"><b>Тонкости в работе</b><br>
                5 лет продуктивной деятельности позволили нам открыть для себя даже мельчайшие тонкости SEO, поэтому мы в точности знаем, чего лишен ваш маркетинг, и что ему нужно для покорения вершин.</div>
            <br>
        </div>
		-->
    </div>
</div>