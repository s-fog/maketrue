<?php
use common\models\Material;
?>
        <div class="materials">
            <div class="materials__inner">
                <?php
                $materials = Material::find()
                    ->limit(9)
                    ->orderBy(['created_at' => SORT_DESC])
                    ->all();
                foreach($materials as $model) {
                    echo $this->render('@frontend/views/material/_item', ['model' => $model]);
                }
                ?>
            </div>
            <?php
            if (count($materials) > 9) {
            ?>
                <div class="loadMore">
                    <span>
                        <img src="/img/load-more-line.png">
                    </span>
                    <div class="loadMore__text">Загрузить еще</div>
                </div>
            <?php } ?>
        </div>
        <br>
        <br>
        <div class="paddingLeft margin content">
                    <div class="header withLine">MakeTrue - Как мы выбрались на рынок <span></span></div>
                    <div class="text">Коллектив MakeTrue - выходцы из крупнейших рекламных агентств Москвы и Санкт-Петербурга. Несколько лет мы существовали, обслуживая крупных клиентов других РА, скрываясь в тени. Накопив сил и денег, вышли на просторный рынок Digital в РФ, большинство обывателей которого уже хорошо нам знакомы.</div>
                    <br>
                    <div class="text"><b>Мы с зубами!</b><br>
                        За 5 лет мы попробовали десятки инструментов, вложив в тесты десятки миллионов рублей. Поэтому, если мы говорим, что какая-либо сторона хромает в вашем маркетинге - будьте добры, прислушайтесь!</div>
                    <br>
                </div>