<?php
use common\models\Brif;

$this->params['seotitle'] = 'seotitle';
$this->params['seodescription'] = 'seodescription';
$this->params['seokeywords'] = 'seokeywords';
$this->params['seoh1'] = 'Бриф на SEO';
?>
<div class="contentWithImage">
    <div class="container">
        <div class="contentWithImage__inner">
            <div class="contentWithImage__left">
                <img src="/img/cimage.png" alt="">
            </div>
            <div class="contentWithImage__right">
                <h1 class="header withLine"><?=$this->params['seoh1']?> <span></span></h1>
                <p>Иногда мы публикуем некоторые важные вещи, например, как часто нужно кормить котиков или как общаться с очень назойливым клиентом, если деньги оплачены работа сделана, а акты подписывать отказываются. На самом деле, опыта и слов очень много, собрать бы их всех воедино. Подписывайтесь на наш RSS, канал.</p>
                <p>Иногда мы публикуем некоторые важные вещи, например, как часто нужно кормить котиков или как общаться с очень назойливым клиентом, если деньги оплачены работа сделана, а акты подписывать отказываются.</p>
            </div>
        </div>
    </div>
</div>

<div class="case" style="padding-top: 1px;">
    <div class="container margin">
        <?=$this->render('@frontend/views/blocks/innovate')?>
    </div>

    <div class="tools">
        <div class="container">
            <div class="tools__inner">
                <?php
                $brifs = Brif::items();
                foreach($brifs as $model) {
                    echo $this->render('@frontend/views/brif/_item', ['model' => $model]);
                }
                ?>
            </div>

            <div class="text-center" style="margin-top: 50px;">
                <div class="button4 openPopup">
                    <span class="button4__text">Или задайте вопрос напрямую</span>
                    <span class="button4__s"></span>
                </div>
            </div>
        </div>
    </div>
</div>