<?php 
use yii\helpers\Url;
?>
<div class="tools__item" style="background-image: url(<?=$model->previewimage?>);">
    <a href="<?=Url::to(['brif/view', 'alias' => $model->alias])?>" class="tools__itemHeader"><span><?=$model->name?></span></a>
    <div class="tools__itemInfo"><?=$model->introtext?></div>
    <div class="text-center">
        <a href="<?=Url::to(['brif/view', 'alias' => $model->alias])?>" class="button2">
            <span class="button2__text">Заполнить</span>
            <span class="button2__s"></span>
        </a>
    </div>
</div>