<?php

use yii\helpers\Url;

$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
$this->params['name'] = $model->name;

?>
<div class="contentWithImage case">
    <div class="container">
        <div class="contentWithImage__inner">
            <div class="contentWithImage__left">
                <img src="<?=$model->image?>" alt="">
            </div>
            <div class="contentWithImage__right">
                <h1 class="header withLine">
                    <?=(!empty($model->seoh1))? $model->seoh1 : $model->name?>
                    <span></span>
                </h1>
                <?=$model->content?>
            </div>
        </div>
        <form class="sendForm brifForm">
            <?=$model->html?>
            <button type="submit" class="button4">
                <span class="button4__text">Отправить Бриф</span>
                <span class="button4__s"></span>
            </button>
            <input type="hidden" name="type" value="Заказан бриф с сайта Maketrue">
            <input type="text" name="BC" class="BC">
            <input type="hidden" name="url" value="<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>">
        </form>

    </div>
</div>

