<?php 
use yii\helpers\Url;
?>
<div class="tools__item tools__item_tool" style="background-image: url(<?=$model->previewimage?>);">
    <a href="<?=Url::to(['tool/view', 'alias' => $model->alias])?>" class="tools__itemHeader"><span><?=$model->name?></span></a>
    <div class="tools__itemInfo"><?=$model->introtext?></div>
    <a href="<?=Url::to(['tool/view', 'alias' => $model->alias])?>" class="tools__itemButton"></a>
</div>