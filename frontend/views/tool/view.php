<?php

use yii\helpers\Url;

$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
$this->params['name'] = $model->name;

?>
<div class="contentWithImage">
    <div class="container">
        <div class="contentWithImage__inner">
            <div class="contentWithImage__left">
                <img src="<?=$model->image?>" alt="">
            </div>
            <div class="contentWithImage__right">
                <h1 class="header withLine">
                    <?=(!empty($model->seoh1))? $model->seoh1 : $model->name?>
                    <span></span>
                </h1>
                <?=$model->content?>
                <div class="contentWithImage__buttons">
                    <a href="<?=Url::to(['brif/view', 'alias' => \common\models\Brif::findOne($model->brif_id)->alias])?>"
                       class="button3"
                       target="_blank">
                        <span class="button3__text">Бриф</span>
                        <span class="button3__s"></span>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?=$model->presentation?>" class="button4 button_pres" target="_blank">
                        <span class="button4__text">Презентация</span>
                        <span class="button4__s"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$model->html_digits?>

<div class="toolBlock">
    <div class="container">
        <?=$model->html?>
    </div>
</div>

<?=$this->render('@frontend/views/blocks/cases')?>