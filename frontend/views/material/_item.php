<?php 
use yii\helpers\Url;
?>
<div class="materials__item">
    <a href="<?=Url::to(['material/view', 'alias' => $model->alias])?>" class="materials__itemImage" style="background-image: url(<?=$model->previewimage?>);"></a>
    <div class="materials__itemContent">
        <div class="materials__itemDate"><?=date("d.m.Y", $model->created_at)?></div>
        <a href="<?=Url::to(['material/view', 'alias' => $model->alias])?>" class="materials__itemName"><span><?=$model->name?></span></a>
        <div class="materials__itemText"><?=$model->introtext?></div>
    </div>
</div>