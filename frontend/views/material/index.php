<?php
use common\models\Brif;

$this->params['seotitle'] = 'Материалы и статьи от MakeTrue | Маркетинг Stream';
$this->params['seodescription'] = '';
$this->params['seokeywords'] = '';
$this->params['seoh1'] = 'Материалы';
?>
<div class="contentWithImage case">
    <div class="container">
        <div class="contentWithImage__inner">
            <div class="contentWithImage__left">
                <img src="/uploads/pic/i1-2.jpg" alt="">
            </div>
            <div class="contentWithImage__right">
                <h1 class="header withLine"><?=$this->params['seoh1']?> <span></span></h1>
                <p>Иногда мы публикуем некоторые размышления, эксперименты и прочие полезные материалы. Если вы активно интересуетесь рынком рекламы, вполне вероятно, данный раздел будет вам полезен. </p>
                <p>Часто публикации являются отражением мыслей сотрудников коллектива MakeTrue. В скором времени, здесь будут появляться материалы, описывающие полный пошаговый процесс работы с реальными проектами. Некоторые статьи содержат описание внутренних механизмом и принципов работа наших маркетинговых инструментов.</p>
            </div>
        </div>
        <br><br>
        <br><br>
        <?=$this->render('@frontend/views/blocks/materials')?>
    </div>
</div>
