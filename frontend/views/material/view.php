<?php

use yii\helpers\Url;

$this->params['seotitle'] = $model->seotitle;
$this->params['seodescription'] = $model->seodescription;
$this->params['seokeywords'] = $model->seokeywords;
$this->params['seoh1'] = $model->seoh1;
$this->params['name'] = $model->name;

?>
<div class="contentWithImage case">
    <div class="container">
        <div class="contentWithImage__inner">
            <div class="contentWithImage__left">
                <img src="<?=$model->image?>" alt="">
            </div>
            <div class="contentWithImage__right content">
                <h1 class="header withLine">
                    <?=(!empty($model->seoh1))? $model->seoh1 : $model->name?>
                    <span></span>
                </h1>
                <?=$model->content?>
            </div>
        </div>
        <div class="content margin">
            <?=$model->content2?>
        </div>
        <p class="materials__itemDate" style="font-size: 18px;">Дата публикации: <?=date("d.m.Y", $model->created_at)?></p>
        <br>
        <br><br><br>
        <div class="materials__itemName">Последние публикации</div>
        <br>
        <br>
        <?=$this->render('@frontend/views/blocks/materials')?>
    </div>
</div>

